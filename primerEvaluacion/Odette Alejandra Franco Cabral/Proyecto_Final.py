import random, time 

miLista = [int(random.randint(1,100)) for i in range (20)]

#METODOS DE ORDENAMIENTO

def Burbuja(p):
        p=miLista
        length=len(p)-1       
        for i in range(0,length):
            print(p)
            for j in range(0, length):
                if p[j] > p [j+1]:
                    aux=p[j]
                    p[j]=p[j+1]
                    p[j+1]=aux
        return p

def baraja (lista):
        for i in range(1,len(lista)):
            p = lista[i]
            j = i-1
            while (j>=0 and lista[j] > p):
             lista[j+1] = lista[j]
             j = j-1
             lista[j+1] = p           
            print(lista)

def shell(dato, lista):
    interval = lista // 2
    while interval > 0:
        for i in range(lista):
            print(dato)
            temp = dato[i]
            j = i
            while j >= interval and dato[j - interval] > temp:
                dato[j] = dato[j - interval]
                j -= interval

            dato[j] = temp
        interval //= 2

def quick(lista):
    izquierda = []
    centro = []
    derecha = []
    if len(lista) > 1:
        p = lista[0]
        for i in lista:
            if i < p:
                izquierda.append(i)
            elif i == p:
                centro.append(i)
            elif i > p:
                derecha.append(i)
        #print(izquierda+["/"]+centro+["/"]+derecha)
        return quick(izquierda)+centro+quick(derecha)
    else:
      return lista

def radix(lista):
        n=0
        for i in lista:
         if len(i) > n:
            n=len(i)

        for j in range(0,len(lista)):
            while len(lista[j]) < n:
                lista[j]= "0" + lista[j]

        for k in range(n-1,-1,-1):
            grupos=[[]for i in range(10)]
            for i in range(len(lista)):
                grupos[int(lista[i][k])].append(lista[i])
            lista=[]
            for l in grupos:
                lista += l
                #print(grupos)
        return [int(i) for i in lista]

def binario(lista):
    for i in range(1, len(lista)):
        print(lista)
        q = lista[i]
        p = busqueda(lista, q, 0, i) + 1
        for k in range(i, p, -1):
            lista[k] = lista[k - 1]
        lista[p] = q 
    print(lista)

def busqueda(lista, p, q, r):
    if r - q <= 1:
        if p < lista[q]:
            return q - 1
        else:
            return q

    mitad = (q + r)//2
    if lista[mitad] < p:
        return busqueda(lista, p, mitad, r)
    elif lista[mitad] > p:
        return busqueda(lista, p, q, mitad)
    else:
        return mitad


def merge(p, q):
    lista=[]
    while (len(p)> 0 and len(q)>0):
        if p[0]<q[0]:
           lista.append(p[0])
           p=p[1:]
        else:
            lista.append(q[0])
            q=q[1:]
            #print(lista)
        
    if len(p)>0:
       lista=lista+p
    if len(q)>0:
        lista=lista+q
    return lista

def ord_merge (lista):
    if len(lista)==1:
        return lista
    izq=lista[:len(lista)//2]
    der=lista[len(lista)//2:]

    izq=ord_merge(izq)
    der=ord_merge(der)
    return merge(izq,der)

#METODOS DE BUSQUEDA

def secuencial(lista,num):
    for i in range(0,len(lista)):
        if num==lista[i]:
          return (" El ingresado si se encuentra en la lista ")          
    return (" El numero no esta en la lista ")


def binaria (lista,numero) :
    interaciones = 0
    for i , item in enumerate (lista) :
        interaciones +=1
        if item ==numero :
            return "Número encontrado en {} pasos en la posición {}." .format (interaciones, i)
    return "Número no encontrado."    

while True:
    Menu = int (input(" Selecciona una opcion : \n 1. Metodos de ordenamiento \n 2. Metodos de busqueda\n  "))

    if Menu == 1:

        metodos = int(input(" Selecciona un metodo de ordenamiento : \n 1. Burbuja \n 2. Baraja \n 3. Shell \n 4. Quicksort \n 5. Radix \n 6. Binario \n 7. Mergesort \n "))
        if metodos == 1:
            inicio=time.time()
            print(f" Lista no Ordenada : \n{miLista}")
            Burbuja(miLista)
            print(f" Lista Ordenada : \n {miLista}")
            final=time.time()
            tiempo= (final-inicio)
            print(f" Tiempo de Ejecucion : {tiempo}")

        elif metodos == 2:  
            inicio=time.time()     
            print(f" Lista sin Ordenar : \n {miLista}")
            baraja(miLista)
            print(f" Lista Ordenada : \n {miLista}")
            final=time.time()
            tiempo= (final-inicio)
            print(f" Tiempo de Ejecucion : {tiempo}")
        elif metodos == 3: 
            inicio=time.time()
            print(f" Lista no Ordenada : \n{miLista}")
            size = len(miLista)
            shell(miLista, size)
            print(f" Lista Ordenada : \n {miLista}")
            final=time.time()
            tiempo= (final-inicio)
            print(f" Tiempo de Ejecucion : {tiempo}")    
        elif metodos == 4:
            inicio=time.time() 
            print(f" Lista sin Ordenar : \n {miLista}")
            print(f" Lista Ordenada : \n {quick(miLista)}") 
            final=time.time()
            tiempo= (final-inicio)
            print(f" Tiempo de Ejecucion : {tiempo}")   
        elif metodos == 5:
            inicio=time.time() 
            miLista = [str(random.randint(1,100)) for i in range (20)]
            print(f" Lista sin Ordenar : \n {miLista}")
            print(f" Lista Ordenada : \n {radix(miLista)}")
            final=time.time()
            tiempo= (final-inicio)
            print(f" Tiempo de Ejecucion : {tiempo}")
        elif metodos == 6:
            inicio=time.time() 
            print(f" Lista sin Ordenar : \n {miLista}")
            binario(miLista)
            print(f" Lista Ordenada : \n {miLista}")   
            final=time.time()
            tiempo= (final-inicio)
            print(f" Tiempo de Ejecucion : {tiempo}")
        elif metodos == 7: 
            inicio=time.time()       
            print(f" Lista no Ordenada : \n{miLista}")
            print(f" Lista Ordenada : \n {ord_merge(miLista)}")
            final=time.time()
            tiempo= (final-inicio)
            print(f" Tiempo de Ejecucion : {tiempo}")
        else:
            print("\n opción no disponible ")    
            

    elif Menu == 2:
        buscar =int(input(" Selecciona un metodo de busqueda : \n 1. Secuencial \n 2. Binaria \n "))
        if buscar == 1:
            inicio=time.time()  
            print (miLista)
            num=int(input(" Ingresa el numero que deseas buscar : \n "))
            busqueda = secuencial(miLista,num)
            print(busqueda)
            final=time.time()
            tiempo= (final-inicio)
            print(f" Tiempo de Ejecucion : {tiempo}")
            
        elif buscar == 2:
            inicio=time.time()  
            print (miLista)
            Num=int(input(" Que número desea buscar en la lista: \n "))
            busqueda = binaria (miLista,Num)
            print(busqueda)
            final=time.time()
            tiempo= (final-inicio)
            print(f" Tiempo de Ejecucion : {tiempo}")
  
